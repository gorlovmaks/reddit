<?php

use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('reddit'));
    }

    /**
     * @When /^перехожу на страницу авторизации$/
     */
    public function IgoToTheAuthorizationPage()
    {
        $this->visit($this->getContainer()->get('router')->generate('sign-in'));
    }

    /**
     * @When /^Я бегаю по страницам$/
     */
    public function IgoToThePopularPosts()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_index_popularposts'));
        $this->visit($this->getContainer()->get('router')->generate('app_index_myposts'));

    }

    /**
     * @When /^я перехожу на страницу регистрации$/
     */
    public function IgoToTheRegisterPage()
    {
        $this->visit($this->getContainer()->get('router')->generate('sign_up'));

    }
    /**
     * @When /^Я заполняю поля данными$/
     * @param TableNode $fields
     */
    public function fillFields(TableNode $fields)
    {
        foreach ($fields->getRowsHash() as $field => $value) {
            $this->fillField($field, $value);
        }

    }

    /**
     * @Then /^Я нажимаю на кнопку"([^"]*)"$/
     * @param $element
     * @throws Exception
     */

    public function iClickOn($element)
    {
        $page = $this->getSession()->getPage();
        $findName = $page->find("css", $element);
        if (!$findName) {
            throw new Exception($element . " could not be found");
        } else {
            $findName->click();
        }
    }

    /**
     *
     * @When /^Я нажимаю на ссылку регистрации объекта"((?:[^"]|\\")*)"$/
     * @param $link
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function clickLink($link)
    {
        $link = $this->fixStepArgument($link);
        $this->getSession()->getPage()->clickLink($link);
    }
}

