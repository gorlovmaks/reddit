<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 * @InheritanceType("JOINED"))
 * @UniqueEntity("email")
 *
 */
class Post
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @var User
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="posts")
     *
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=254,unique=true)
     */
    private $post_name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }




    public function __construct()
    {
        $this->users = new ArrayCollection();
    }


    public function addUser(User $user)
    {
        $this->users->add($user);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return mixed
     */
    public function getPostName()
    {
        return $this->post_name;
    }

    /**
     * @param mixed $post_name
     * @return Post
     */
    public function setPostName($post_name)
    {
        $this->post_name = $post_name;
        return $this;
    }
}
