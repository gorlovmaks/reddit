<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 07.07.18
 * Time: 11:49
 */

namespace App\Controller;


use App\Entity\User;

use App\Form\RegisterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{

    /**
     * @Route("/register", name="sign_up")
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ObjectManager $manager
     * @return Response
     */
    public function registerUser(
        Request $request,
        UserHandler $userHandler,
        ObjectManager $manager
    )
    {
        $user = new User();

        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = [
                'email' => $user->getEmail(),
                'password' => $user->getPassword(),
                'roles' => $user->getRoles(),
            ];
            $user = $userHandler->createNewUser($data);

            $manager->persist($user);
            $manager->flush();

          return $this->redirectToRoute('reddit');

        }

        return $this->render('registerForm.html.twig', [
            'form'  => $form->createView()
        ]);

    }

    /**
     * @Route("/auth", name="sign-in")
     * @param Request $request
     * @param UserHandler $userHandler
     * @param UserRepository $userRepository
     * @param LoggerInterface $logger
     * @return Response
     */
    public function authAction(
        Request $request,
        UserHandler $userHandler,
        UserRepository $userRepository
    )
    {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                md5($data['password']),
                $data['email']
            );

            dump($user);
            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('reddit');
            }

        }

        return $this->render(
            'auth.html.twig', [
            'form' => $form->createView()
        ]);
    }
}