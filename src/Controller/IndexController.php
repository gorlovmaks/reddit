<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 07.07.18
 * Time: 11:46
 */

namespace App\Controller;


use App\Entity\Post;
use App\Model\Api\ApiContext;
use App\Repository\PostRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{

    /**
     * @Route("/", name="reddit")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function indexAction(
        Request $request,
        ApiContext $apiContext
    ){
        $posts = [];
        $form = $this->createForm('App\Form\FiltrationType');
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();

            $result = $apiContext->getFiltrationPicture($data);
            $posts = $result['data']['children'];
            dump($posts);
        }

        return $this->render('index.html.twig',[
            'form' => $form->createView(),
            'posts' => $posts
        ]);

    }

    /**
     * @Route("/concrete_post", name="app_post")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function linkPost(
        Request $request,
        ApiContext $apiContext
    ){

            $data = ['name' => $request->request->get('post_id')];
            $post = $apiContext->getPicture($data);

        return $this->render('concrete_post.html.twig',[
            'post' => $post
        ]);
    }

    /**
     * @Route("/concrete_post/{name}",requirements={"name": "\w+"}, name="add_post")
     * @param $name
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addPost($name, ObjectManager $manager){
        $user = $this->getUser();
        $post = new Post();

        $post->addUser($user);
        $post->setPostName($name);
        $user->addPost($post);

        $manager->persist($post);
        $manager->persist($user);
        $manager->flush();

        return $this->redirectToRoute('app_index_myposts');
    }

    /**
     * @Route("/my_posts")
     *
     * @param PostRepository $postRepository
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function myPosts(
        PostRepository $postRepository,
        ApiContext $apiContext
    ){
        $post_names = [];
        $user = $this->getUser();
        $posts_in_repository = $postRepository->getMyPosts($user->getId());
        foreach ($posts_in_repository as $value){
            $post_names [] = $value['post_name'];
        }



        $posts = $apiContext->getPosts($post_names);

        return $this->render('my_posts.html.twig',[
            'my_posts' => $posts
        ]);
    }

    /**
     * @Route("/popular_post")
     *
     * @param PostRepository $postRepository
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function popularPosts(
        PostRepository $postRepository,
        ApiContext $apiContext
    )
    {
        $post_names = [];
        $popular_posts = $postRepository->getPopularPosts();

        foreach ($popular_posts as $value){
            $post_names [] = $value['post_name'];
        }


        $posts = $apiContext->getPosts($post_names);

        return $this->render('popular_posts.html.twig',[
            'my_posts' => $posts
        ]);

    }

}