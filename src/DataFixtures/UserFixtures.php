<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Entity\Post;
use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {
        $post1 = new Post();
        $post1->setPostName('t3_8vyvrn');

            $manager->persist($post1);

        $post2 = new Post();
        $post2->setPostName('t3_8qgug8');

            $manager->persist($post2);

        $post3 = new Post();
        $post3->setPostName('t3_8pubxo');

            $manager->persist($post3);

        $post4 = new Post();
        $post4->setPostName('t3_8p7kfe');

            $manager->persist($post4);

        $post5 = new Post();
        $post5->setPostName('t3_8oqnfq');

            $manager->persist($post5);

        $post6 = new Post();
        $post6->setPostName('t3_8nxof3');

            $manager->persist($post6);

        $post7 = new Post();
        $post7->setPostName('t3_8na7a4');

            $manager->persist($post7);

        $post8 = new Post();
        $post8->setPostName('t3_8kb3n2');

            $manager->persist($post8);




        $user = $this->userHandler->createNewUser([
            'email' => '123@123.ru',
            'password' => '123'
        ]);

        $user->addPost($post1);
        $user->addPost($post2);
        $user->addPost($post3);
        $user->addPost($post4);
        $user->addPost($post5);
        $user->addPost($post6);
        $user->addPost($post7);


        $manager->persist($user);

        $manager->flush();



        $user2 = $this->userHandler->createNewUser([
            'email' => '321@321.ru',
            'password' => '123'
        ]);

        $user2->addPost($post1);
        $user2->addPost($post2);
        $user2->addPost($post7);


        $manager->persist($user2);

        $manager->flush();
    }
}
