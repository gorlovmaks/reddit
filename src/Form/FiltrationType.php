<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FiltrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q',TextType::class,['label' => 'Что ищем?'])
            ->add('sort',ChoiceType::class,array(
                'choices' => array(
                    'По релевантности(relevance)' => 'relevance',
                    'Горячие(hot)' => 'hot',
                    'В топе(top)' => 'top',
                    'Новые(new)' => 'new',
                    'Комментируемые(comments)' => 'comments',
                )))
            ->add('limit',ChoiceType::class,array(
                'choices' => array(
                    '8шт' => 8,
                    '12шт' => 12,
                    '16шт' => 16,
                    '50шт' => 50,
                    '100шт' => 100,
                )))
            ->add('save', SubmitType::class);
    }
}
