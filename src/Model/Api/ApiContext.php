<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_REDDIT = 'https://www.reddit.com/r/picture/search.json?q={value_q}&sort={value_sort}&limit={value_limit}&type=link ';
    const ENDPOINT_CONCRETE_POST = 'https://www.reddit.com/api/info.json?id={name}';
    const ENDPOINT_MY_POSTS = 'https://www.reddit.com/api/info.json?id={name_posts}';

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function getFiltrationPicture(array $data)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_REDDIT, [
            'value_q' => $data['q'],
            'value_sort' => $data['sort'],
            'value_limit' => $data['limit']
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function getPosts(array $data)
    {
        $data_name = implode(",", $data);
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_MY_POSTS, [
            'name_posts' => $data_name
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function getPicture(array $data)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_POST, [
            'name' => $data['name'],
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }
}
