<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 07.07.18
 * Time: 11:37
 */

namespace App\Model\User;


use App\Entity\User;
use App\Model\Api\ApiContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{

    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var ApiContext
     */
    private $apiContext;

    public function __construct(
        ContainerInterface $container, ApiContext $apiContext
    )
    {
        $this->container = $container;
    }

    public function makeUserSession(User $user) {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }

    /**
     * @param array $data
     * @return User
     */
    public function createNewUser(array $data) {
        $password = md5($data['password']);
        $user = new User();
        $user->setEmail($data['email']);
        $user->setPassword($password);

        return $user;
    }
}